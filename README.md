# CMDBC

Improving country via BlockChain
Installation and setup instructions:
----------

1. Install python 3.7
2. Install pip3
3. Add paths to pip and python 3.7 in environment path variables

.. code-block:: text

    pip install Flask version 1.0.3
    pip install virtualenv
    virtualenv venv
    mypthon\Scripts\activate


4. Install pycharm
5. Open pycharm, select 'Git' from the 'Check out from version control option'
6. Provide credential and copy the repository in local machine
7. Go to 'run', select edit configurations 
8. Follow following steps to setup the configuration
-> click + on the top left corner
-> select python
-> give 'CDB' as name
-> locate blockchain.py path in script path and press Ok
9. Run the project
10. open  http://127.0.0.1:5000/ in browser
